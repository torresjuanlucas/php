<?php

require_once 'db.php';

// only allow logged in users past this point
if (!$_SESSION['user']) {
    die("<p>Authorized users only. You must <a href=login.php>login</a> to access this page.</p>");
}
$userId = $_SESSION['user']['id'];

function printForm($values = array('travellerId' => '', 'departureDate' => '', 'fromCity' => '', 'toCity' => '', 'transportation ' => '')) {
    // here-doc
    $td = $values['travellerId'];
    $dd = $values['departureDate'];
    $fc = $values['fromCity'];
    $tc = $values['toCity'];
    $t = $values['transportation'];
    $form = <<< ROSESAREBEST
<form method="post">
    travellerId: <input type="text" name="travellerId" value="$td"><br>
    departureDate: <input type="text" name="departureDate" value="$dd"><br>
    fromCity: <input type="text" name="fromCity" value="$fc"><br>
    toCity: <input type="text" name="toCity" value="$tc"><br>
    transportation: <input type="text" name="transportation" value="$t"><br>
    <input type="submit" value="Add trip">
</form>
ROSESAREBEST;
    echo $form;
}

if (isset($_POST['travellerId'])) {
    // extract submission
    $travellerId = $_POST['travellerId'];
    $departureDate = $_POST['departureDate'];
    $fromCity = $_POST['fromCity'];
    $toCity = $_POST['toCity'];
    $transportation = $_POST['transportation'];
    $values = $_POST;
    //
    $errorList = array();
    
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo "</ul>\n";
        printForm($values);
    } else {
        // successful submission
        // FIXME: SQL injection possible here !!! CYA policy applies
        $sql = sprintf("INSERT INTO trips VALUES (NULL, NULL, '%s', '%s', '%s', '%s')",
                $travellerId, 
                mysqli_real_escape_string($link, $travellerId),
                mysqli_real_escape_string($link, $departureDate),
                mysqli_real_escape_string($link, $fromCity),
                mysqli_real_escape_string($link, $toCity),
                mysqli_real_escape_string($link, $transportation));
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL query error: " . mysqli_error($link));
        }
        $id = mysqli_insert_id($link);
        echo "<p>Trip has been saved</p>\n";
    }
} else {
    // STATE 1: first show
    printForm();
}

