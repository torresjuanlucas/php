<?php

/* edit_delete_denied.html.twig */
class __TwigTemplate_2fd73bc0e68d9794ac041fa9249770e67bc9147d201437935fd1170158ed575d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "edit_delete_denied.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "User Denied";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    
    <p>User may only edit/delete their own to-dos.<a href=\"/index\">Go home</a> or <a href=\"/add\">add to-do</a>.</p>

";
    }

    public function getTemplateName()
    {
        return "edit_delete_denied.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}User Denied{% endblock %}

{% block content %}
    
    <p>User may only edit/delete their own to-dos.<a href=\"/index\">Go home</a> or <a href=\"/add\">add to-do</a>.</p>

{% endblock %}", "edit_delete_denied.html.twig", "D:\\XAMPP\\htdocs\\ipd\\slimtodo\\templates\\edit_delete_denied.html.twig");
    }
}
