<?php

session_start(); // enable sessions mechanisms

if (!isset($_SESSION['count'])) {
    $_SESSION['count'] = 0; 
}

$_SESSION['count']++;

echo "Count is " . $_SESSION['count'];

