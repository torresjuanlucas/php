<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

session_start();

require_once 'vendor/autoload.php';

DB::$user = 'slimtodoalex';
DB::$password = 'mug4n2Fms5lBrQQG';
DB::$dbName = 'slimtodoalex';
DB::$encoding = 'utf8';
//
//DB ERROR HANDLING
DB::$error_handler = 'sql_error_handler';
db::$nonsql_error_handler = 'non_sql_error_handler';

//SQL ERROR HANDLING FUNCTION
function sql_error_handler($params) {
    global $app, $log;
    $app->render('error_internal.html.twig');
    $log->err("Error: " . $params['error']);
    $log->err("Query: " . $params['query']);
    http_response_code(500);
    die; // don't want to keep going if a query broke
}

//
function non_sql_error_handler($params) {
    global $app, $log;
    $app->render('error_internal.html.twig');
    $log->err("Error: " . $params['error']);
    http_response_code(500);
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}
//to be able to display username on webpage. 
//ex: You are logged in as "user"
$twig = $app->view()->getEnvironment();
$twig->addGlobal('userSession', $_SESSION['user']);

//
//EMAIL ALREADY REGISTERED
$app->get('/isemailregistered/:email', function($email) use($app) {
    $row = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    echo!$row ? "" : '<span style="background-color: red; font-weight: bold;">Email already in system</span>';
});
//
//REGISTRATION FIRST SHOW
$app->get('/register', function() use($app) {
    $app->render('register.html.twig');
});
//
//REGISTRATION SUBMISSION
$app->post('/register', function() use ($app) {
//extract submission
    $name = $app->request()->post('name');
    $email = $app->request()->post('email');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    $values = array('name' => $name, 'email' => $email);
    $errorList = array();
//name check
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorList, "Name must be between 2 and 50 characters.");
        $value['name'] = '';
    }
// email check
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email must look like a valid email.");
        $values['email'] = '';
    } else {
        $row = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        if ($row) {
            array_push($errorList, "Email already used.");
            $values['email'] = '';
        }
    }
// matching passwords check
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords don't match");
    } else {
        if (strlen($pass1) < 2 || strlen($pass2) > 50) {
            array_push($errorList, "Password must be between 2 and 50");
        }
    }
//
//password pattern check
    if (!preg_match('/[a-z]/', $pass1) || !preg_match('/[a-z]/', $pass1) || !preg_match('/[0-9' . preg_quote("!@#\$%^&*()_-+={}[],.<>;:'\"~`") . ']/', $pass1)) {
        array_push($errorList, "Password must have at least one lowercase, one uppercase, and one number.");
    }
//
    if ($errorList) { // 3. failed submission
        $app->render('register.html.twig', array(
            'errorList' => $errorList,
            'v' => $values));
    } else { //2. successful submission
//encrypted password
        $passEnc = password_hash($pass1, PASSWORD_BCRYPT);
        DB::insert('users', array('name' => $name, 'email' => $email, 'password' => $passEnc));
        $app->render('register_success.html.twig', array('name' => $name, 'email' => $email, 'password' => $passEnc));
    }
});
//
//
//LOGIN FIRST SHOW
$app->get('/login', function() use($app) {
    $app->render('login.html.twig');
});

//LOGIN SUBMISSION
$app->post('/login', function() use ($app) {
//extract data
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');

    $row = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    $error = false;

    if (!$row) {
        $error = true; //user not found
    } else { //password verify
        if (!password_verify($password, $row['password'])) { //password failed
            $error = true;
        }
    }
    if ($error) {
        $app->render('login.html.twig', array('error' => true));
    } else {
        unset($row['password']);
        $_SESSION['user'] = $row;
        $app->render('login_success.html.twig', array('userSession' => $_SESSION['user'], 'email' => $email));
    }
});

//check logged in users
$app->get('/session', function() {
    print_r($_SESSION);
});

//logout 
$app->get('/logout', function() use ($app) {
    $_SESSION['user'] = array();
    $app->render('logout.html.twig', array('userSession' => $_SESSION['user']));
});
//
//
//
//INDEX 
//load to main page when nothing entered
$app->get('/', function() use ($app) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
    $todoList = DB::query("SELECT * FROM todos WHERE ownerId=%i", $_SESSION['user']['id']);
    $app->render('index.html.twig', array('list' => $todoList));
});
//
//INDEX PAGE--SHOW TABLE IF USER IS LOGGED IN, ELSE TELL USER TO LOGIN
$app->get('/index', function() use($app) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
//to display list with user's name
    // $todoList = DB::query("SELECT name, task, date, isDone, todos.id as id, users.id as userId FROM todos, users WHERE todos.ownerId=users.id AND todos.ownerId=%i", $_SESSION['user']['id']);
//to display the to-do without the user's name
    $todoList = DB::query("SELECT * FROM todos WHERE ownerId=%i", $_SESSION['user']['id']);
    $app->render('index.html.twig', array('list' => $todoList));
});



// add/edit todos first show
$app->get('/:op(/:id)', function($op, $id = -1) use ($app) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
//
    if (($op == 'add' && $id != -1) || ($op == 'edit' && $id == -1)) {
        echo 'INVALID REQUEST'; //FIXME on Monday - display standard 404 from slim
        return;
    }
//
    if ($id != -1) {
        $values = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i", $id);
        if (!$values) {
            echo "NOT FOUND"; //FIXME on Monday - display standard 404 from slim
            return;
        }
    } else {// nothing to load from database - adding
        $values = array();
    }
    $app->render('todo_addedit.html.twig', array(
        'v' => $values,
        'isEditing' => ($id != -1)
    ));
})->conditions(array(
    'op' => ('edit|add'),
    'id' => '\d+'
));
//
// ADD/EDIT SUBMISSION
$app->post('/:op(/:id)', function($op, $id = -1) use ($app, $log) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
    if (($op == 'add' && $id != -1) || ($op == 'edit' && $id == -1)) {
        echo 'INVALID REQUEST'; //FIXME on Monday - display standard 404 from slim
        return;
    }
//
//extract submission
    $task = $app->request()->post('task');
    $date = $app->request()->post('date');
    $isDone = $app->request()->post('isDone');
//
    $values = array('task' => $task, 'date' => $date, 'isDone' => $isDone);
    $errorList = array();
//
    if (strlen($task) < 2 || strlen($task) > 50) {
        array_push($errorList, "Task must be between 2 and 50 characters.");
        $values['task'] = '';
    }


    if (date_parse($date) == FALSE) {
        array_push($errorList, "Date format");
        $values['date'] = '';
    }

    if (!$isDone == "pending" || !$isDone == "done") {
        array_push($errorList, "Status must be selected");
        $values['isDone'] = '';
    }
    //TODO IMAGE CHECK
    $todoImage = array();
    if ($_FILES['todoImage']['error'] != UPLOAD_ERR_NO_FILE) {
        $todoImage = $_FILES['todoImage'];
        //check for errors
        if ($todoImage['error'] != 0) {
            array_push($errorList, "Error uploading file.");
            $log->err("Error uploading file: " . print_r($todoImage, true));
        } else {
            if(strstr($todoImage['name'], '..')){
                array_push($errorList, "Invalid file name");
                $log->warn("Uploaded file name with .. in it (possible attack) " . print_r($todoImage, true));
            }
            //TODO: CHECK IF FILE ALREADY EXISTS, CHECK MAXIMUM SIZE OF THE FILE, DIMENSIONS OF THE IMAGE, ETC.
            $info = getimagesize($todoImage["tmp_name"]);
            if ($info == FALSE) {
                array_push($errorList, "File doesn't look like a valid image.");
            } else {
                if ($info['mime'] == 'image/jpeg' || $info['mime'] == 'image/png' || $info['mime'] == 'image/gif') {
                    //image type is valid- all good
                } else {
                    array_push($errorList, "Image must be a JPG, GIF, or PNG only.");
                }
            }
        }
    } else { //no file uploaded
        if ($op == 'add') {
            array_push($errorList, "Image is required when creating new product.");
        }
    }
//
    if ($errorList) { // 3. failed submission
        $app->render('todo_addedit.html.twig', array(
            'errorList' => $errorList,
            'v' => $values));
    } else { //2. successful submission
         if($todoImage){ //   '[^a-zA-Z0-9_\.-]' 
       // $sanitizedFileName = preg_replace('[^a-zA-Z0-9_\.-]', '_', $todoImage['name']);
        $imagePath = 'uploads/' . $todoImage['name'];  // $todoImage['name']
        if (!move_uploaded_file($todoImage['tmp_name'], $imagePath)){
            $log->err(sprintf("Error moving uploaded file: " . print_r($todoImage, true)));
            $app->render('error_internal.html.twig');
            return;
        }
        //TODO: if EDITING and new file is uploaded we should delete the old one in uploads
        $values['imagePath'] = "/" . $imagePath;
        }
//UPDATE
        if ($id != -1) {
//VERIFY USER MATCHES ORIGINAL TO-DO WRITER
            $row = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i", $id);
            if ($row['ownerId'] == $_SESSION['user']['id']) {
                DB::update('todos', $values, "id=%i", $id);
            } else { //access denied
                $app->render('access_denied.html.twig');
                return;
            }
        } else {
//INSERT STATEMENT
            DB::insert('todos', array('ownerId' => $_SESSION['user']['id'], 'task' => $task, 'date' => $date, 'isDone' => $isDone, 'imagePath' => $imagePath));
        }
        $app->render('todo_addedit_success.html.twig', array('isEditing' => ($id != -1)));
    }
})->conditions(array(
    'op' => ('edit|add'),
    'id' => '\d+'
));

//
// TO-DO DELETE FIRST SHOW
$app->get('/delete/:id', function($id) use ($app) {
//VERIFY USER MATCHES ORIGINAL TO-DO WRITER
    $todo = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i", $id);
    if (!$_SESSION['user'] || $todo['ownerId'] != $_SESSION['user']['id']) {
        $app->render('access_denied.html.twig');
        return;
    }
    if (!$todo) {
        $app->render('not_found.html.twig');
        return;
    }
    $app->render('todo_delete.html.twig', array('t' => $todo));
});


//DELETE FORM SUBMISSION
$app->post('/delete/:id', function($id) use ($app) {
//VERIFY USER MATCHES ORIGINAL TO-DO WRITER
    $row = DB::queryFirstRow("SELECT * FROM todos WHERE id=%i", $id);
    if (!$_SESSION['user'] || $_SESSION['user']['id'] != $row['ownerId']) {
        $app->render('access_denied.html.twig');
        return;
    }
    $confirmed = $app->request()->post('confirmed');
    if ($confirmed != 'true') {
        $app->render('not_found.html.twig');
        return;
    }
    DB::delete('todos', "id=%i", $id);
    if (DB::affectedRows() == 0) {
        $app->render('not_found.html.twig');
    } else {
        $app->render('todo_delete_success.html.twig');
    }
});
//
//show list of todos with owner name
$app->get('/list', function() {
    $todoList = DB::query("SELECT name, task, date, isDone FROM todos AS t, users AS u");
    print_r($todoList);
});


//NEEDS TO BE AT BOTTOM TO RUN THE PHP
$app->run();
