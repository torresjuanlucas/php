<?php

/* post_add_success.html.twig */
class __TwigTemplate_d774fda3b046606e60c99d086a3a3b1b1703410b987fe7de8e9c54db8856c7e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "post_add_success.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " Post Added";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    
    <p>Post added.<br> <a href=\"/post/new\">Add another post</a> or <a href=\"/\"> view list of post</a></p>

";
    }

    public function getTemplateName()
    {
        return "post_add_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %} Post Added{% endblock %}

{% block content %}
    
    <p>Post added.<br> <a href=\"/post/new\">Add another post</a> or <a href=\"/\"> view list of post</a></p>

{% endblock %}", "post_add_success.html.twig", "C:\\xampp\\htdocs\\ipd\\quiz2socialAlex\\templates\\post_add_success.html.twig");
    }
}
