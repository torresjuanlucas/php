<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <meta charset="UTF-8">
        <title></title>
        <script>
            $(document).ready(function() {
                $('input[name=username]').keyup(function() {
                    // AJAX request
                    var username = $('input[name=username]').val();
                    $('#isTaken').load('isusernametaken.php?username=' + username);
                });
            });
        </script>
    </head>
    <body>
        <div id="centeredContent">
            <?php
            require_once 'db.php';

// for debuggin only
// print_r($_POST);

            function printForm($values = array('username' => '', 'email' => '')) {
                // here-doc
                $u = $values['username'];
                $e = $values['email'];
                $form = <<< ROSESAREBEST
<form method="post">
    Username: <input type="text" name="username" value="$u"><span id="isTaken"></span><br>
    Password: <input type="password" name="pass1"><br>
    Password (repated): <input type="password" name="pass2"><br>
    Email: <input type="text" name="email" value="$e"><br>
    <input type="submit" value="Register">
</form>
ROSESAREBEST;
                echo $form;
            }

            if (isset($_POST['passport'])) {
                // extract submission
                $passport = $_POST['passport'];
                $password = $_POST['password'];
                $values = $_POST;
                //
                $errorList = array();
                if (!preg_match('/[a-zA-Z]{2}[0-9]{6}/', $passport)) {
                    array_push($errorList, "passport not valid");
                    $values['passport'] = "";
                } else { // passport seems valid - check if it is available
                    $sql = sprintf("SELECT * FROM travellers WHERE passport='%s'",
                            mysqli_real_escape_string($link, $passport));
                    $result = mysqli_query($link, $sql);
                    if (!$result) {
                        die("SQL query error: " . mysqli_error($link));
                    }
                    $row = mysqli_fetch_assoc($result);
                    if ($row) {
                        array_push($errorList, "passport already taken.");
                        $values['passport'] = "";
                    }
                }
                
                if (strlen($password) < 8 || strlen($password) > 30) {
                        array_push($errorList, "Password is too short/long, must be between 8 and 30 characters");
                    }// \ + * ? [ ^ ] $ ( ) { } = ! < > | : -
                    if (!preg_match('/[A-Z]/', $password) || !preg_match('/[a-z]/', $password) || !preg_match('/[0-9' . preg_quote("!@#\$%^&*()_-+={}[],.<>;:'\"~`") . ']/', $password)) {
                        array_push($errorList, "Password must include at least one character in each three categories: "
                                . "uppercase letter, lowercase letter, digit or special character");
                    }
                
                // array with 1 or more elements is considered "True" value
                if ($errorList) {
                    // errors - failed submission
                    echo "<p>Your submission has problems:</p>\n";
                    echo "<ul>\n";
                    foreach ($errorList as $error) {
                        echo "<li>$error</li>\n";
                    }
                    echo "</ul>\n";
                    printForm($values);
                } else {
                    // successful submission
                    // FIXME: SQL injection possible here !!! CYA policy applies
                    $passEnc = password_hash($password, PASSWORD_BCRYPT);
                    $sql = sprintf("INSERT INTO travellers VALUES (NULL, '%s', '%s')", mysqli_real_escape_string($link, $username), mysqli_real_escape_string($link, $passEnc), mysqli_real_escape_string($link, $email));
                    $result = mysqli_query($link, $sql);
                    if (!$result) {
                        die("SQL query error: " . mysqli_error($link));
                    }
                    echo "<p>You've been added</p>\n";
                }
            } else {
                // STATE 1: first show
                printForm();
            }
            ?>

        </div>
    </body>
</html>

