<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <meta charset="UTF-8">
        <title></title>
        <script>
            $(document).ready(function() {
                $('input[name=username]').keyup(function() {
                    //ajax request
                    var username = $('input[name=username]').val();
                    $('#isTaken').load('isusernametaken.php?username=' + username);
                });
            });
        </script>
    </head>
    <body>
        <div id="centeredContent">
<?php

require_once 'db.php';

// for debuggin only
// print_r($_POST);

function printForm($values = array('username' => '', 'email' => '')) {
    // here-doc
    $u = $values['username'];
    $e = $values['email'];
    $form = <<< ROSESAREBEST
<form method="post">
    Username: <input type="text" name="username" value="$u"><span id="isTaken"></span><br>
    Password: <input type="password" name="pass1"><br>
    Password (repated): <input type="password" name="pass2"><br>
    Email: <input type="text" name="email" value="$e"><br>
    <input type="submit" value="Register">
</form>
ROSESAREBEST;
    echo $form;
}

if (isset($_POST['username'])) {
    // extract submission
    $username = $_POST['username'];
    $pass1 = $_POST['pass1'];
    $pass2 = $_POST['pass2'];
    $email = $_POST['email'];
    $values = $_POST;
    //
    $errorList = array();
    if (!preg_match('/^[A-Za-z0-9_]{5,20}$/', $username)) {
        array_push($errorList, "Name must be between 2 and 50 characters long"
                . "and consist of upper/lower case characters digits and underscore only.");
        $values['username'] = "";
    }
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords do not match");
    } else {
        if (strlen($pass1) < 6 || strlen($pass1) > 100) {
            array_push($errorList, "Passwords too short/long, must be between 6 and 100 characters");
        }// \ + * ? [ ^ ] $ ( ) { } = ! < > | : -
        if (!preg_match('/[A-Z]/', $pass1) || !preg_match('/[a-z]/', $pass1) 
                || !preg_match('/[0-9' . preg_quote("!@#\$%^&*()_-+={}[],.<>;:'\"~`") . ']/', $pass1) ) {
            array_push($errorList,
                    "Password must include at least one character in each three categories: "
                    . "uppercase letter, lowercase letter, digit or special character");
        }
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        array_push($errorList, "Email seems invalid");
    }
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
        }
        echo "</ul>\n";
        printForm($values);
    } else {
        // successful submission
        // FIXME: SQL injection possible here !!! CYA policy applies
        $passEnc = password_hash($pass1, PASSWORD_BCRYPT);
        $sql = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
                mysqli_real_escape_string($link, $username),
                mysqli_real_escape_string($link, $passEnc),
                mysqli_real_escape_string($link, $email));
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL query error: " . mysqli_error($link));
        }
        echo "<p>You've been added: Name $username and $email</p>\n";
    }
} else {
    // STATE 1: first show
    printForm();
}



?>

            </div>
    </body>
</html>
