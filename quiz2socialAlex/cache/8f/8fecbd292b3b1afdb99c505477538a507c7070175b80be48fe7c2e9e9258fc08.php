<?php

/* post_add.html.twig */
class __TwigTemplate_45163d56ff01bd9e0f0ed8585a7de1afb5ae3f13e491a979053b4605da3c0807 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "post_add.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "New Post";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
    ";
        // line 11
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 12
            echo "        <ul>
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 14
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "        </ul>
    ";
        }
        // line 18
        echo "
    <form method=\"post\" enctype=\"multipart/form-data\">
        Title: <input type=\"text\" name=\"title\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "title", array()), "html", null, true);
        echo "\"><br>
        Body: <textarea name=\"body\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "body", array()), "html", null, true);
        echo "</textarea><br>             
            <input type=\"submit\" value=\"Add post\">
        </form>

        ";
    }

    public function getTemplateName()
    {
        return "post_add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 21,  67 => 20,  63 => 18,  59 => 16,  50 => 14,  46 => 13,  43 => 12,  41 => 11,  38 => 10,  35 => 9,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}New Post{% endblock %}
{#  {% block headextra %} 
  <script type=\"text/javascript\">
  \$(\"#isDone\").val(\"<?php echo \$_POST['isDone'];?>\");
  </script>
  {% endblock %} #}
{% block content %}

    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\" enctype=\"multipart/form-data\">
        Title: <input type=\"text\" name=\"title\" value=\"{{v.title}}\"><br>
        Body: <textarea name=\"body\">{{v.body}}</textarea><br>             
            <input type=\"submit\" value=\"Add post\">
        </form>

        {% endblock %}", "post_add.html.twig", "C:\\xampp\\htdocs\\ipd\\quiz2socialAlex\\templates\\post_add.html.twig");
    }
}
