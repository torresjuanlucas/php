<?php

require_once 'db.php';

// for debuggin only
// print_r($_SERVER);

function printForm() {
    $selfUrl = $_SERVER['REQUEST_URI'];
    $form = <<< ROSESAREBEST
<form method="post" action="$selfUrl">
    Username: <input type="text" name="passport"><br>
    Password: <input type="password" name="password"><br>
    <input type="submit" value="Login">
</form>
ROSESAREBEST;
    echo $form;
}

if (isset($_POST['passport'])) {
    // extract submission
    $username = $_POST['passport'];
    $password = $_POST['password'];
    //
    $error = false;
    $sql = sprintf("SELECT * FROM travellers WHERE passport='%s'",
            mysqli_real_escape_string($link, $passport));
    $result = mysqli_query($link, $sql);
    if (!$result) {
        die("SQL query error: " . mysqli_error($link));
    }
    $row = mysqli_fetch_assoc($result);
    if ($row) {
        if (!password_verify($password, $row['password'])) {
            $error = true;
        }
    } else {
        $error = true;
    }
    // array with 1 or more elements is considered "True" value
    if ($error) {
        // error - failed submission
        echo "<p>Login failed, try again or <a href=\"register.php\">register</a></p>\n";
        printForm($values);
    } else {
        // successful submission
        unset($row['password']);
        $_SESSION['user'] = $row;
        echo "<p>You've been logged in</p>";
    }
} else {
    // STATE 1: first show
    printForm();
}

