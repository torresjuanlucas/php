<?php

/* register.html.twig */
class __TwigTemplate_bad9f3f9ef7bb6cc18cfd77633e467e37a8c076747f1d37693fa4259450ab0c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "register.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headextra' => array($this, 'block_headextra'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Register";
    }

    // line 4
    public function block_headextra($context, array $blocks = array())
    {
        echo " 
        <script>
            \$(document).ready(function () {
                \$('input[name=email]').bind(\"propertychange change click keyup input paste\", function () {
                    //AJAX request
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load('/isemailregistered/' + email);
                });
            });
        </script>
 ";
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        // line 17
        echo "            ";
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 18
            echo "                <ul>
                    ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 20
                echo "                        <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "                </ul>
            ";
        }
        // line 24
        echo "
            <form method=\"post\" enctype=\"multipart/form-data\">
                Name: <input type=\"text\" name=\"name\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "name", array()), "html", null, true);
        echo "\"><br>
                Email: <input type=\"text\" name=\"email\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "email", array()), "html", null, true);
        echo "\"><span id=\"isTaken\"></span><br>
                Password:  <input type=\"password\" name=\"pass1\"><br>
                Password (repeated): <input type=\"password\" name=\"pass2\"><br>
                Photo Path: <input type=\"file\" name=\"profileImage\"><br>
                <input type=\"submit\" value=\"Register\">
            </form>

   ";
    }

    public function getTemplateName()
    {
        return "register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 27,  82 => 26,  78 => 24,  74 => 22,  65 => 20,  61 => 19,  58 => 18,  55 => 17,  52 => 16,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Register{% endblock %}
{% block headextra %} 
        <script>
            \$(document).ready(function () {
                \$('input[name=email]').bind(\"propertychange change click keyup input paste\", function () {
                    //AJAX request
                    var email = \$('input[name=email]').val();
                    \$('#isTaken').load('/isemailregistered/' + email);
                });
            });
        </script>
 {% endblock %}

 {% block content %}
            {% if errorList %}
                <ul>
                    {% for error in errorList %}
                        <li>{{error}}</li>
                        {% endfor %}
                </ul>
            {% endif %}

            <form method=\"post\" enctype=\"multipart/form-data\">
                Name: <input type=\"text\" name=\"name\" value=\"{{v.name}}\"><br>
                Email: <input type=\"text\" name=\"email\" value=\"{{v.email}}\"><span id=\"isTaken\"></span><br>
                Password:  <input type=\"password\" name=\"pass1\"><br>
                Password (repeated): <input type=\"password\" name=\"pass2\"><br>
                Photo Path: <input type=\"file\" name=\"profileImage\"><br>
                <input type=\"submit\" value=\"Register\">
            </form>

   {% endblock %}", "register.html.twig", "C:\\xampp\\htdocs\\ipd\\quiz2socialAlex\\templates\\register.html.twig");
    }
}
