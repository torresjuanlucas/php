<?php

/* index.html.twig */
class __TwigTemplate_e16371dd43bfeac20bdb9bbb398aedf062ceba023825fc407025c14042464ac0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Post List";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "     <form class=\"searchForm\" method=\"get\">
         Search:<input type=\"text\" name=\"search\">
         <input type=\"submit\" value=\"Search\">
     </form>
                <p><a href=\"/post/new\">Add Post</a></p>
                    
                    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["l"]) {
            echo "                      
                       <img style=\"float: left;\" src=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "photoPath", array(), "array"), "html", null, true);
            echo "\" width=\"40\">                          
                       <p>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "name", array()), "html", null, true);
            echo " posted on ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "datePosted", array()), "html", null, true);
            echo "</p>
                       <h2>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "title", array()), "html", null, true);
            echo "</h2>  
                       <p>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["l"], "body", array()), "html", null, true);
            echo "</p>
                        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 18
            echo "                            <tr><td colspan=\"6\">No posts to display</td></tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['l'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "                        
                </table>
            ";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 20,  74 => 18,  67 => 16,  63 => 15,  57 => 14,  53 => 13,  46 => 12,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Post List{% endblock %}

 {% block content %}
     <form class=\"searchForm\" method=\"get\">
         Search:<input type=\"text\" name=\"search\">
         <input type=\"submit\" value=\"Search\">
     </form>
                <p><a href=\"/post/new\">Add Post</a></p>
                    
                    {% for l in list %}                      
                       <img style=\"float: left;\" src=\"{{l['photoPath']}}\" width=\"40\">                          
                       <p>{{l.name}} posted on {{l.datePosted}}</p>
                       <h2>{{l.title}}</h2>  
                       <p>{{l.body}}</p>
                        {% else %}
                            <tr><td colspan=\"6\">No posts to display</td></tr>
                        {% endfor %}
                        
                </table>
            {% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\ipd\\quiz2socialAlex\\templates\\index.html.twig");
    }
}
