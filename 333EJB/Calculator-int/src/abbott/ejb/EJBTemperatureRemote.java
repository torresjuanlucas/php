/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abbott.ejb;

import javax.ejb.Remote;

/**
 *
 * @author 1795849
 */
@Remote
public interface EJBTemperatureRemote {

    double celToFah(double cel);
    
}
