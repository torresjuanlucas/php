<?php

require_once 'db.php';

if (!isset($_GET['passport'])) {
    die("Error: passport parameter missing.");
}
$username = $_GET['passport'];

$sql = sprintf("SELECT id FROM travellers WHERE passport='%s'", mysqli_real_escape_string($link, $passport));
$result = mysqli_query($link, $sql);
if (!$result) {
    die("SQL query error: " . mysqli_error($link));
}

$row = mysqli_fetch_assoc($result);
if ($row) {
    echo '<span style="background-color: red; font-weight: bold; ">passport already taken</span>';
} else {
    echo "";
}

