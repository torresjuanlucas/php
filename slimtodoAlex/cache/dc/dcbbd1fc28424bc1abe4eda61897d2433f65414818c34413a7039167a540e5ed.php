<?php

/* todo_addedit_success.html.twig */
class __TwigTemplate_9de290ba3dd6963c46af4ca3119c6c75c914a8c8569ab05df81973866d881795 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "todo_addedit_success.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo " To-Do ";
        if (($context["isEditing"] ?? null)) {
            echo "edited";
        } else {
            echo "added";
        }
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    
    <p>To-Do ";
        // line 7
        if (($context["isEditing"] ?? null)) {
            echo "updated";
        } else {
            echo "added";
        }
        echo ".<br> <a href=\"/add\">Add another to-do</a> or <a href=\"/index\"> view list of to</a></p>

";
    }

    public function getTemplateName()
    {
        return "todo_addedit_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 7,  43 => 6,  40 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %} To-Do {% if isEditing %}edited{% else %}added{% endif %}{% endblock %}

{% block content %}
    
    <p>To-Do {% if isEditing %}updated{% else %}added{% endif %}.<br> <a href=\"/add\">Add another to-do</a> or <a href=\"/index\"> view list of to</a></p>

{% endblock %}", "todo_addedit_success.html.twig", "D:\\XAMPP\\htdocs\\ipd\\slimtodo\\templates\\todo_addedit_success.html.twig");
    }
}
