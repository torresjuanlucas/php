<?php

/* post_addedit.html.twig */
class __TwigTemplate_ad456b8b5cbbac67e483338f417c1ae8d4a2c047ed2798c0cede8f5445eb9c47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "post_addedit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Add post";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <form method=\"post\" enctype=\"multipart/form-data\">
        Title: <input type=\"text\" name=\"title\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "title", array()), "html", null, true);
        echo "\"><br>
        Body: <input type=\"text\" name=\"body\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "body", array()), "html", null, true);
        echo "\"><br>
        datePosted: <input type=\"date\" name=\"datePosted\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "datePosted", array()), "html", null, true);
        echo "\"><br>
        <input type=\"submit\" value=\"Add post\">
    </form>
";
    }

    public function getTemplateName()
    {
        return "post_addedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 9,  45 => 8,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Add post{% endblock %}

{% block content %}
    <form method=\"post\" enctype=\"multipart/form-data\">
        Title: <input type=\"text\" name=\"title\" value=\"{{v.title}}\"><br>
        Body: <input type=\"text\" name=\"body\" value=\"{{v.body}}\"><br>
        datePosted: <input type=\"date\" name=\"datePosted\" value=\"{{v.datePosted}}\"><br>
        <input type=\"submit\" value=\"Add post\">
    </form>
{% endblock %}
", "post_addedit.html.twig", "C:\\xampp\\htdocs\\ipd\\quiz2social\\templates\\post_addedit.html.twig");
    }
}
