<?php

/* index.html.twig */
class __TwigTemplate_e16371dd43bfeac20bdb9bbb398aedf062ceba023825fc407025c14042464ac0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Index";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        if ((isset($context["memberSession"]) ? $context["memberSession"] : null)) {
            // line 7
            echo "        <table border=\"1\">
            <tr><th>#</th><th>Post</th><th>Title</th><th>Body</th><th>DatePosted</th></tr>
        ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["postList"]) ? $context["postList"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 10
                echo "            <tr>
                <td>";
                // line 11
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "</td>
                <td>";
                // line 12
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "title", array()), "html", null, true);
                echo "</td>
                <td>";
                // line 13
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "body", array()), "html", null, true);
                echo "</td>
                <td>";
                // line 14
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "datePosted", array()), "html", null, true);
                echo "</td>
                <td>
                    <a href=\"/edit/";
                // line 16
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["t"]) ? $context["t"] : null), "id", array()), "html", null, true);
                echo "\">Edit</a>
                    <a href=\"/delete/";
                // line 17
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["t"]) ? $context["t"] : null), "id", array()), "html", null, true);
                echo "\">Delete</a>
                </td>
            </tr>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "        </table>
    ";
        } else {
            // line 23
            echo "        <p>You must register or login first to view your your data.</p>
    ";
        }
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 23,  104 => 21,  86 => 17,  82 => 16,  77 => 14,  73 => 13,  69 => 12,  65 => 11,  62 => 10,  45 => 9,  41 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Index{% endblock %}

{% block content %}
    {% if memberSession %}
        <table border=\"1\">
            <tr><th>#</th><th>Post</th><th>Title</th><th>Body</th><th>DatePosted</th></tr>
        {% for p in postList %}
            <tr>
                <td>{{loop.index}}</td>
                <td>{{p.title}}</td>
                <td>{{p.body}}</td>
                <td>{{p.datePosted}}</td>
                <td>
                    <a href=\"/edit/{{t.id}}\">Edit</a>
                    <a href=\"/delete/{{t.id}}\">Delete</a>
                </td>
            </tr>
        {% endfor %}
        </table>
    {% else %}
        <p>You must register or login first to view your your data.</p>
    {% endif %}
{% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\ipd\\quiz2social\\templates\\index.html.twig");
    }
}
