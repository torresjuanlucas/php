/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package todo.ejb;

import javax.ejb.Remote;
import todo.entities.TodoItem;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 1795849
 */
@Remote

public interface Quiz1TodoDataEJBRemote {

    @EJB
    public TodoItem[] getAllTodoItems();

    public void addTodoItem(TodoItem item);
}
