<?php

/* index.html.twig */
class __TwigTemplate_e16371dd43bfeac20bdb9bbb398aedf062ceba023825fc407025c14042464ac0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "To-Do List";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "                <p><a href=\"/add\">Add To-Do</a></p>
                <table border=\"3\">
                    <tr><th>#</th><th>Task</th><th>Date</th><th>Status</th><th>Image</th><th>Actions</th></tr>
                    
                    ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 11
            echo "                        <tr class=\"";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                echo "rowodd";
            } else {
                echo "roweven";
            }
            echo "\">
                            <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td> ";
            // line 13
            echo "                            <td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "name", array(), "array"), "html", null, true);
            echo "</td>
                            <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "task", array()), "html", null, true);
            echo "</td>
                            <td>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "date", array(), "array"), "html", null, true);
            echo "</td>
                            <td>";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "isDone", array(), "array"), "html", null, true);
            echo "</td>
                            <td><img src=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "imagePath", array(), "array"), "html", null, true);
            echo "\" width=\"150\"</td>
                            <td>                              
                                <a href=\"/delete/";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "\">Delete</a>
                                <a href=\"/edit/";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "\">Edit</a>
                            </td>                             
                        </tr>
                        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 24
            echo "                            <tr><td colspan=\"6\">No to-dos to display</td></tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "                        
                </table>
            ";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 26,  117 => 24,  100 => 20,  96 => 19,  91 => 17,  87 => 16,  83 => 15,  79 => 14,  74 => 13,  71 => 12,  62 => 11,  44 => 10,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}To-Do List{% endblock %}

 {% block content %}
                <p><a href=\"/add\">Add To-Do</a></p>
                <table border=\"3\">
                    <tr><th>#</th><th>Task</th><th>Date</th><th>Status</th><th>Image</th><th>Actions</th></tr>
                    
                    {% for t in list %}
                        <tr class=\"{% if loop.index is odd %}rowodd{% else %}roweven{% endif %}\">
                            <td>{{loop.index}}</td> {# either p.name or p['name'] both work #}
                            <td>{{t['name']}}</td>
                            <td>{{t.task}}</td>
                            <td>{{t['date']}}</td>
                            <td>{{t['isDone']}}</td>
                            <td><img src=\"{{t['imagePath']}}\" width=\"150\"</td>
                            <td>                              
                                <a href=\"/delete/{{t.id}}\">Delete</a>
                                <a href=\"/edit/{{t.id}}\">Edit</a>
                            </td>                             
                        </tr>
                        {% else %}
                            <tr><td colspan=\"6\">No to-dos to display</td></tr>
                        {% endfor %}
                        
                </table>
            {% endblock %}", "index.html.twig", "C:\\xampp\\htdocs\\ipd\\slimtodo\\templates\\index.html.twig");
    }
}
