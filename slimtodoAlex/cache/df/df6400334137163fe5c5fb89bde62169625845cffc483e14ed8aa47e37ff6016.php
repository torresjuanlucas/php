<?php

/* todo_addedit.html.twig */
class __TwigTemplate_48e0a97bfa5af125472eb682a1f9b8cd6ab5d5143acae06a0dfa7e6c3936eecd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "todo_addedit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "To-Do ";
        if ((isset($context["isEditing"]) ? $context["isEditing"] : null)) {
            echo "edit";
        } else {
            echo "add";
        }
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
    ";
        // line 11
        if ((isset($context["errorList"]) ? $context["errorList"] : null)) {
            // line 12
            echo "        <ul>
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorList"]) ? $context["errorList"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 14
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "        </ul>
    ";
        }
        // line 18
        echo "
    <form method=\"post\" enctype=\"multipart/form-data\">
        Task: <input type=\"text\" name=\"task\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "task", array()), "html", null, true);
        echo "\"><br>
        Date: <input type=\"date\" name=\"date\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "date", array()), "html", null, true);
        echo "\"><br>
        Status: <select name=\"isDone\" id=\"isDone\">
                    <option value=\"pending\" ";
        // line 23
        if (($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "isDone", array()) == "pending")) {
            echo "selected";
        }
        echo ">Pending</option>
                    <option value=\"done\" ";
        // line 24
        if (($this->getAttribute((isset($context["v"]) ? $context["v"] : null), "isDone", array()) == "done")) {
            echo "selected";
        }
        echo ">Done</option>
                </select><br>
        Image Path: <input type=\"file\" value=\"";
        // line 26
        if ((isset($context["isEditing"]) ? $context["isEditing"] : null)) {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["v"]) ? $context["v"] : null), "imagePath", array()), "html", null, true);
        } else {
            echo "''";
        }
        echo "\"  name=\"todoImage\"><br>
            <input type=\"submit\" value=\"";
        // line 27
        if ((isset($context["isEditing"]) ? $context["isEditing"] : null)) {
            echo "Update";
        } else {
            echo "Add";
        }
        echo " To-Do\">
        </form>

        ";
    }

    public function getTemplateName()
    {
        return "todo_addedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 27,  94 => 26,  87 => 24,  81 => 23,  76 => 21,  72 => 20,  68 => 18,  64 => 16,  55 => 14,  51 => 13,  48 => 12,  46 => 11,  43 => 10,  40 => 9,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}To-Do {% if isEditing %}edit{% else %}add{% endif %}{% endblock %}
{#  {% block headextra %} 
  <script type=\"text/javascript\">
  \$(\"#isDone\").val(\"<?php echo \$_POST['isDone'];?>\");
  </script>
  {% endblock %} #}
{% block content %}

    {% if errorList %}
        <ul>
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}

    <form method=\"post\" enctype=\"multipart/form-data\">
        Task: <input type=\"text\" name=\"task\" value=\"{{v.task}}\"><br>
        Date: <input type=\"date\" name=\"date\" value=\"{{v.date}}\"><br>
        Status: <select name=\"isDone\" id=\"isDone\">
                    <option value=\"pending\" {% if v.isDone == 'pending' %}selected{% endif %}>Pending</option>
                    <option value=\"done\" {% if v.isDone == 'done' %}selected{% endif %}>Done</option>
                </select><br>
        Image Path: <input type=\"file\" value=\"{% if isEditing %}{{v.imagePath}}{% else %}''{% endif %}\"  name=\"todoImage\"><br>
            <input type=\"submit\" value=\"{% if isEditing %}Update{% else %}Add{% endif %} To-Do\">
        </form>

        {% endblock %}", "todo_addedit.html.twig", "C:\\xampp\\htdocs\\ipd\\slimtodo\\templates\\todo_addedit.html.twig");
    }
}
