<?php

require_once 'vendor/autoload.php';

DB::$dbName = 'people';
DB::$user = 'people';
DB::$encoding = 'utf-8';
DB::$password = 'k5IoFlhvbobaEfqG';

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

$app->get('/hello/:name', function ($name) {
    echo "Hello, " . $name;
});

$app->get('/hello/:name/:age', function ($name, $age) use ($app) {
    DB::insert('persons', array('name' => $name, 'age' => $age));
    $app->render('hello.html.twig', array('name' => $name, 'age' => $age));
});

$app->get('/addperson', function() use ($app) {
    // 1. First show
    $app->render('addperson.html.twig');
});

$app->post('/addperson', function() use ($app) {
    // receiving a submission
    $name = $app->request()->post('name');
    $age = $app->request()->post('age');
    $values = array('name' => $name, 'age' => $age);
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorList, "Name must be between 2 and 50 characters long");
        $values['name'] = '';
    }
    if (empty($age) || $age < 0 || $age > 150) {
        array_push($errorList, "Age must be between 1 and 150");
        $values['age'] = '';
    }
    if ($errorList) {
        // 3. failed submission
        $app->render('addperson.html.twig', array(
            'errorList' => $errorList,
            'v' => $values));
    } else {
        // 2. successful submission
        DB::insert('persons', array('name' => $name, 'age' => $age));
        $app->render('addperson_success.html.twig', array('name' => $name, 'age' => $age));
    }
});



$app->run();
