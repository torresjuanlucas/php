<?php


use Monolog\Logger;
use Monolog\Handler\StreamHandler;

session_start();

require_once 'vendor/autoload.php';

DB::$user = 'quiz2social';
//DB::$password = 'AAnTggdjKHkpOETH'; // house DB
DB::$password = 'Pdfbl64GKksXzoxK'; //  college DB
DB::$dbName = 'quiz2social';
DB::$encoding = 'utf8';
//
//DB ERROR HANDLING
DB::$error_handler = 'sql_error_handler';
db::$nonsql_error_handler = 'non_sql_error_handler';

//SQL ERROR HANDLING FUNCTION
function sql_error_handler($params) {
    global $app, $log;
    $app->render('error_internal.html.twig');
    $log->err("Error: " . $params['error']);
    $log->err("Query: " . $params['query']);
    http_response_code(500);
    die; // don't want to keep going if a query broke
}

//
function non_sql_error_handler($params) {
    global $app, $log;
    $app->render('error_internal.html.twig');
    $log->err("Error: " . $params['error']);
    http_response_code(500);
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}
//to be able to display username on webpage. 
//ex: You are logged in as "user"
$twig = $app->view()->getEnvironment();
$twig->addGlobal('userSession', $_SESSION['user']);

//
//EMAIL ALREADY REGISTERED
$app->get('/isemailregistered/:email', function($email) use($app) {
    $row = DB::queryFirstRow("SELECT * FROM members WHERE email=%s", $email);
    echo!$row ? "" : '<span style="background-color: red; font-weight: bold;">Email already in system</span>';
});
//
//REGISTRATION FIRST SHOW
$app->get('/register', function() use($app) {
    $app->render('register.html.twig');
});
//
//REGISTRATION SUBMISSION
$app->post('/register', function() use ($app, $log) {
//extract submission
    $name = $app->request()->post('name');
    $email = $app->request()->post('email');
    $pass1 = $app->request()->post('pass1');
    $pass2 = $app->request()->post('pass2');
    $values = array('name' => $name, 'email' => $email);
    $errorList = array();
//
    if (strlen($name) < 2 || strlen($name) > 50) {
        array_push($errorList, "Name must be between 2 and 50 characters.");
        $value['name'] = '';
    }
//
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        array_push($errorList, "Email must look like a valid email.");
        $values['email'] = '';
    } else {
        $row = DB::queryFirstRow("SELECT * FROM members WHERE email=%s", $email);
        if ($row) {
            array_push($errorList, "Email already used.");
            $values['email'] = '';
        }
    }
//
    if ($pass1 != $pass2) {
        array_push($errorList, "Passwords don't match");
    } else {
        if (strlen($pass1) < 2 || strlen($pass2) > 50) {
            array_push($errorList, "Password must be between 2 and 50");
        }
    }
//
//password pattern check
    if (!preg_match('/[a-z]/', $pass1) || !preg_match('/[a-z]/', $pass1) || !preg_match('/[0-9' . preg_quote("!@#\$%^&*()_-+={}[],.<>;:'\"~`") . ']/', $pass1)) {
        array_push($errorList, "Password must have at least one lowercase, one uppercase, and one number.");
    }
//
    //POST IMAGE CHECK
    $profileImage = array();
    if ($_FILES['profileImage']['error'] != UPLOAD_ERR_NO_FILE) {
        $profileImage = $_FILES['profileImage'];
        //check for errors
        if ($profileImage['error'] != 0) {
            array_push($errorList, "Error uploading file.");
            $log->err("Error uploading file: " . print_r($profileImage, true));
        } else {
            if (strstr($profileImage['name'], '..')) {
                array_push($errorList, "Invalid file name");
                $log->warn("Uploaded file name with .. in it (possible attack) " . print_r($profileImage, true));
            }
            //TODO: CHECK IF FILE ALREADY EXISTS, CHECK MAXIMUM SIZE OF THE FILE, DIMENSIONS OF THE IMAGE, ETC.
            $info = getimagesize($profileImage["tmp_name"]);
            if ($info == FALSE) {
                array_push($errorList, "File doesn't look like a valid image.");
            } else {
//                //CHECK IMAGE SIZE, 
                if (filesize($profileImage["tmp_name"]) > 200000) {
                    array_push($errorList, "Image must be smaller than 20kb.");
                }
                //CHECK IMAGE DIMENSIONS
                if ($info[0] > 300 || $info[1] > 300) {
                    array_push($errorList, "Image must not be bigger than 300x300 pixels.");
                }
                if ($info['mime'] == 'image/jpeg' || $info['mime'] == 'image/png' || $info['mime'] == 'image/gif') {
                    //image type is valid- all good
                } else {
                    array_push($errorList, "Image must be a JPG, GIF, or PNG only.");
                }
            }
        }
    } else { //no file uploaded
        array_push($errorList, "Photo must be uploaded with new registration.");
    }


    if ($errorList) { // 3. failed submission
        $app->render('register.html.twig', array(
            'errorList' => $errorList,
            'v' => $values));
    } else { //2. successful submission
        if ($profileImage) { //   '[^a-zA-Z0-9_\.-]' 
          //  $sanitizedFileName = preg_replace('[^a-zA-Z0-9_\.-]', '_', $profileImage['name']); Greg's code but he never checked it, doesn't work
            $imagePath = 'uploads/' . $profileImage['name'];  // 
            if (!move_uploaded_file($profileImage['tmp_name'], $imagePath)) {
                $log->err(sprintf("Error moving uploaded file: " . print_r($profileImage, true)));
                $app->render('error_internal.html.twig');
                return;
            }
            //TODO: if EDITING and new file is uploaded we should delete the old one in uploads
            $values['photoPath'] = "/" . $imagePath;
        }
//encrypted password
        $passEnc = password_hash($pass1, PASSWORD_BCRYPT);
        $values['password'] = $passEnc;
        DB::insert('members', $values);
        $app->render('register_success.html.twig', $values);
    }
});
//
//
//
//LOGIN FIRST SHOW
$app->get('/login', function() use($app) {
    $app->render('login.html.twig');
});

//LOGIN SUBMISSION
$app->post('/login', function() use ($app) {
//extract data
    $email = $app->request()->post('email');
    $password = $app->request()->post('password');

    $row = DB::queryFirstRow("SELECT * FROM members WHERE email=%s", $email);
    $error = false;

    if (!$row) {
        $error = true; //user not found
    } else { //password verify
        if (!password_verify($password, $row['password'])) { //password failed
            $error = true;
        }
    }
    if ($error) {
        $app->render('login.html.twig', array('error' => true));
    } else {
        unset($row['password']);
        $_SESSION['user'] = $row;
        $app->render('login_success.html.twig', array('userSession' => $_SESSION['user'], 'email' => $email));
    }
});

//check logged in users
$app->get('/session', function() {
    print_r($_SESSION);
});

//logout 
$app->get('/logout', function() use ($app) {
    $_SESSION['user'] = array();
    $app->render('logout.html.twig', array('userSession' => $_SESSION['user']));
});
//
//
//INDEX 
//load to main page when nothing entered
$app->get('/(:term)', function($term = null) use ($app) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    } 
    if(!isset($_GET['search'])){
    $postList = DB::query("SELECT name, title, body, datePosted, photoPath, posts.id as id, members.id as memberId FROM posts, members WHERE posts.authorId=members.id");
    $app->render('index.html.twig', array('list' => $postList));
    } else {
        $term = $app->request()->get('search');
         $postList = DB::query("SELECT name, title, body, datePosted, photoPath, posts.id as id, members.id as memberId FROM posts, members WHERE posts.authorId=members.id AND title LIKE '%$term%' OR body LIKE '%$term%' AND posts.authorId=members.id");
         $app->render('index.html.twig', array('list' => $postList));
    }
})->conditions(array(
    'term' => '\w'
));
//
//
//
$app->get('/user/:id', function($id = -1) use($app) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
    if ($id == -1) {
        $app->render('not_found.html.twig');
        return;
    }
    if ($id != -1) {
//to display list with user's name
        $postList = DB::query("SELECT name, title, body, datePosted, photoPath FROM posts, members WHERE posts.authorId=members.id AND posts.authorId=%i", $id);
//to display the post without the user's name
        $app->render('index.html.twig', array('list' => $postList));
    }
})->conditions(array(
    'id' => '\d+'
));


// add post first show
$app->get('/post/new', function() use ($app, $log) {
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
    $app->render('post_add.html.twig');
});
//
///
//
// ADD SUBMISSION
$app->post('/post/new', function() use ($app, $log) {
    //if user isnt logged in, deny access
    if (!$_SESSION['user']) {
        $app->render('access_denied.html.twig');
        return;
    }
//extract submission
    $authorId = $_SESSION['user']['id'];
    $title = $app->request()->post('title');
    $body = $app->request()->post('body');
//
    $values = array('title' => $title, 'body' => $body);
    $errorList = array();
// title check
    if (strlen($title) < 1 || strlen($title) > 100) {
        array_push($errorList, "Post must be between 1 and 100 characters.");
        $values['title'] = '';
    }
//body check
    if (strlen($body) < 1 || strlen($body) > 2000) {
        array_push($errorList, "Body must be between 1 and 2000 characters.");
        $values['body'] = '';
    }
//
    if ($errorList) { // 3. failed submission
        $app->render('post_add.html.twig', array(
            'errorList' => $errorList,
            'v' => $values));
    } else { //2. successful submission
//INSERT STATEMENT
        DB::insert('posts', array('authorId' => $authorId, 'title' => $title, 'body' => $body));

        $app->render('post_add_success.html.twig');
    }
});


$app->run();
