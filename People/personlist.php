<?php

require_once 'db.php';

$sql = "SELECT * FROM persons";
$result = mysqli_query($link, $sql);
if (!$result) {
    die("SQL query error: " . mysqli_error($link));
}

while ($row = mysqli_fetch_assoc($result)) {
    printf("<p>ID: %s, name: %s, age: %s</p>\n", $row['id'], $row['name'], $row['age']);
}
