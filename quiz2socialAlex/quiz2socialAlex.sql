-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2017 at 06:00 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz2social`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(100) NOT NULL,
  `photoPath` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `email`, `password`, `photoPath`) VALUES
(1, 'Eric Andre', 'ericAndre5@gmail.com', '$2y$10$65Xz79LpNGrvibNTD3kKM.5seplxKgcjToa8WbCaw2zxmj5YbalQy', '/uploads/21317974_1460520340732082_765673730978526055_n.jpg'),
(2, 'Rusty Shackleford', 'Rusty5@gmail.com', '$2y$10$aoab3rZ3VKF1okx4zd9Drue65y67jIukh2FhqBJl5HgdPI2Vp5ljS', '/uploads/bf1.jpg'),
(3, 'MariaDB', 'MariaDB5@gmail.com', '$2y$10$/S8WanTc5XR3zMGYg1l0x.QNkaWSU9pgFcMXzLbc5degAERvPP89e', '/uploads/stranger-things.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `body` varchar(2000) NOT NULL,
  `datePosted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `authorId`, `title`, `body`, `datePosted`) VALUES
(1, 2, 'new post', 'first post', '2017-11-01 15:38:56'),
(2, 2, 'attempt 3', 'trying to get user/id to work', '2017-11-01 16:06:17'),
(3, 2, 'attempt 4', 'works or not', '2017-11-01 16:08:14'),
(4, 2, 'user id not working', 'user id not adding properly', '2017-11-01 16:19:52'),
(5, 1, 'Eric\'s first post', 'first post', '2017-11-01 16:32:01'),
(6, 3, 'Halloween is over', 'Halloween has come and gone once again', '2017-11-01 16:52:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `authorId` (`authorId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `fk_memberId` FOREIGN KEY (`authorId`) REFERENCES `members` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
