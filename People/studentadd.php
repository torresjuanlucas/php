<?php

require_once 'db.php';

// for debuggin only
// print_r($_GET);

function printForm($values = array('name' => '', 'gpa' => '', 'yob' => '')) {
    // here-doc
    $n = $values['name'];
    $g = $values['gpa'];
    $y = $values['yob'];
    $form = <<< ROSESARECOOL
<form>
    Name: <input type="text" name="name" value="$n"><br>
    Gpa: <input type="number" name="gpa" step="0.01" value="$g"><br>
    Yob: <input type="number" name="yob" value="$y"><br>
    <input type="submit" value="Add student">
</form>
ROSESARECOOL;
    echo $form;
}

if (isset($_GET['name'])) {
    // extract submission
    $name = $_GET['name'];
    $gpa = $_GET['gpa'];
    $yob = $_GET['yob'];
    
    $values = $_GET;
    //
    $errorList = array();
    if (strlen($name) < 2 || (strlen($name) > 50)) {
        array_push($errorList, "Name must be between 2 and 50 characters long");
        $values['name'] = "";
    }
    if ($gpa == '' || $gpa < 0 || $gpa > 4.3) {
        array_push($errorList, "gpa must be between 0 and 4.3");
        $values['gpa'] = "";
    }
    if ($yob < 1900 || $yob > 2100) {
        array_push($errorList, "year of birth must be between 1900 and 2100");
        $values['yob'] = "";
    }
    // array with 1 or more elements is considered "True" value
    if ($errorList) {
        // errors - failed submission
        echo "<p>Your submission has problems:</p>\n";
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>\n";
//            printForm($values);
        }
        echo "</ul>\n";
        printForm($values);
        
    }  else {
        // successful submission
        // fixme: sql injection possible here!!! CYA policy applies
        $sql = sprintf("INSERT INTO students VALUES (NULL, '%s', '%s', '%s')", 
                mysqli_real_escape_string($link, $name),
                mysqli_real_escape_string($link, $gpa),
                mysqli_real_escape_string($link, $yob));
        $result = mysqli_query($link, $sql);
        if (!$result){
            die("SQL query error: " . mysqli_error($link));
        }
        echo "<p>You've been added: student Name $name, the pga is $gpa and the year of birth is $yob</p>\n";
    }
} else {
    // STATE 1: first show
    printForm();
}