/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abbott.ejb;

import javax.ejb.Stateless;

/**
 *
 * @author 1795849
 */
@Stateless
public class EJBTemperature implements EJBTemperatureRemote {

    @Override
    public double celToFah(double cel) {
        return cel * 9/5 + 32;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
