<?php

/* admin/products_delete.html.twig */
class __TwigTemplate_bbcc07e56e35134e2778e9d4f372fd781de4d9fb4a324d47a79a007f4e86174c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "admin/products_delete.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Confirm delete";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <p><b>Are you sure you want to delete this product?</b></p>
    <form action=\"/admin/products/list\" style=\"display: inline;\">
        <input type=\"submit\" value=\"Cancel\">
    </form>
    ";
        // line 12
        echo "    <form method=\"post\"  style=\"display: inline;\">
        <input type=\"hidden\" name=\"confirmed\" value=\"true\">
        <input type=\"submit\" value=\"Delete\">
    </form>
    <div>
        <p>Name: ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "name", array()), "html", null, true);
        echo "</p>
        <p>Description: ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "description", array()), "html", null, true);
        echo "</p>
        <p>Name: ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "price", array()), "html", null, true);
        echo "</p>
        <img src=\"\">
    </div>


";
    }

    public function getTemplateName()
    {
        return "admin/products_delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 19,  55 => 18,  51 => 17,  44 => 12,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Confirm delete{% endblock %}

{% block content %}
    <p><b>Are you sure you want to delete this product?</b></p>
    <form action=\"/admin/products/list\" style=\"display: inline;\">
        <input type=\"submit\" value=\"Cancel\">
    </form>
    {# action not requried since we're already on URL looking like
        action=\"/admin/products/delete/{{p.id}}\" #}
    <form method=\"post\"  style=\"display: inline;\">
        <input type=\"hidden\" name=\"confirmed\" value=\"true\">
        <input type=\"submit\" value=\"Delete\">
    </form>
    <div>
        <p>Name: {{p.name}}</p>
        <p>Description: {{p.description}}</p>
        <p>Name: {{p.price}}</p>
        <img src=\"\">
    </div>


{% endblock %}
", "admin/products_delete.html.twig", "C:\\xampp\\htdocs\\ipd\\slimshop\\templates\\admin\\products_delete.html.twig");
    }
}
