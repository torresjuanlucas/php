<?php

/* todo_delete.html.twig */
class __TwigTemplate_04df560a1b408fa2e6303d9dd51b4d3f84bacf9ca19625ba265844e34d0b2fcd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "todo_delete.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Confirm Delete";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "     <p><b>Are you sure you want to delete this todo?</b></p>
     <form action=\"/index\" style=\"display:inline;\">
         <input type=\"submit\" value=\"Cancel\">
     </form>
     ";
        // line 12
        echo "       <form method=\"post\" style=\"display:inline;\">
           <input type=\"hidden\" name=\"confirmed\" value=\"true\">
         <input type=\"submit\" value=\"Delete\">
     </form>
           
           <div>
               <p>Task: ";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["t"]) ? $context["t"] : null), "task", array()), "html", null, true);
        echo "</p>
               <p>Date: ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["t"]) ? $context["t"] : null), "date", array()), "html", null, true);
        echo "</p>
               <p>Status: ";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["t"]) ? $context["t"] : null), "isDone", array()), "html", null, true);
        echo "</p>
               <img src=\"\">
           </div>
               
   ";
    }

    public function getTemplateName()
    {
        return "todo_delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 20,  56 => 19,  52 => 18,  44 => 12,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Confirm Delete{% endblock %}

 {% block content %}
     <p><b>Are you sure you want to delete this todo?</b></p>
     <form action=\"/index\" style=\"display:inline;\">
         <input type=\"submit\" value=\"Cancel\">
     </form>
     {# action not necessary since we're already on URL
        looking like action=\"/admin/products/delete/{{p.id}}\" #}
       <form method=\"post\" style=\"display:inline;\">
           <input type=\"hidden\" name=\"confirmed\" value=\"true\">
         <input type=\"submit\" value=\"Delete\">
     </form>
           
           <div>
               <p>Task: {{t.task}}</p>
               <p>Date: {{t.date}}</p>
               <p>Status: {{t.isDone}}</p>
               <img src=\"\">
           </div>
               
   {% endblock %}", "todo_delete.html.twig", "C:\\xampp\\htdocs\\ipd\\slimtodo\\templates\\todo_delete.html.twig");
    }
}
