<?php

/* master.html.twig */
class __TwigTemplate_00c8b13873e5ffcab63f19c339c6109e5a631d51d6c5ae928fb64f80e6565d4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'headextra' => array($this, 'block_headextra'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/styles.css\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>    
        <title>";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 9
        $this->displayBlock('headextra', $context, $blocks);
        // line 10
        echo "</head>
<body>
    <div id=\"centeredContent\">
        ";
        // line 13
        if (($context["userSession"] ?? null)) {
            // line 14
            echo "            <p>You're logged in as ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["userSession"] ?? null), "name", array()), "html", null, true);
            echo ". You may <a href=\"/logout\">logout</a></p>
        ";
        } else {
            // line 16
            echo "            <p>You're not logged in. <a href=\"/login\">Login</a> or <a href=\"/register\">register</a></p>
        ";
        }
        // line 18
        echo "    ";
        $this->displayBlock('content', $context, $blocks);
        // line 19
        echo "</div>
</body>
</html>";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
        echo "Default";
    }

    // line 9
    public function block_headextra($context, array $blocks = array())
    {
    }

    // line 18
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 18,  68 => 9,  62 => 8,  56 => 19,  53 => 18,  49 => 16,  43 => 14,  41 => 13,  36 => 10,  34 => 9,  30 => 8,  22 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# created Oct 28 2017 #}
<!DOCTYPE>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/styles.css\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>    
        <title>{% block title %}Default{% endblock %}</title>
    {% block headextra %}{% endblock %}
</head>
<body>
    <div id=\"centeredContent\">
        {% if userSession %}
            <p>You're logged in as {{userSession.name}}. You may <a href=\"/logout\">logout</a></p>
        {% else %}
            <p>You're not logged in. <a href=\"/login\">Login</a> or <a href=\"/register\">register</a></p>
        {% endif %}
    {% block content %}{% endblock %}
</div>
</body>
</html>", "master.html.twig", "D:\\XAMPP\\htdocs\\ipd\\slimtodo\\templates\\master.html.twig");
    }
}
