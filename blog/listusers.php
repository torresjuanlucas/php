<?php

require_once 'db.php';

$sql = "SELECT * FROM users";
$result = mysqli_query($link, $sql);
if (!$result) {
    die("SQL query error: " . mysqli_error($link));
}

echo "<table border=1><tr><th>ID</th><th>username</th><th>password</th><th>email</th></tr>\n";

while ($row = mysqli_fetch_assoc($result)) {
    // print_r($row);
    printf("<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>\n",
            $row['id'], $row['username'], $row['password'], $row['email']);
}
echo "</table>";
