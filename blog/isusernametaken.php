<?php

require_once 'db.php';

if (isset($_GET['username'])) {
    die("Error: username parameter missing.");
}
$username = $_GET['username'];

$sql = sprintf("SELECT id FROM users WHERE username='%s'", mysqli_real_escape_string($link, $username));
$result = mysqli_query($link, $sql);
if (!$result) {
    die("SQL query error: " . mysqli_error($link));
}

$row = mysqli_fetch_assoc($result);
if ($row) {
    echo '<span style="background-color: red; font-weight: bold; ">Username already taken</span>';
} else {
    echo "";
}
