<?php

/* admin/products_list.html.twig */
class __TwigTemplate_305bdc7fc96c8562869be882777e91f7a92c06e14e56ee6bd239db0e47eb74ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "admin/products_list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Products list";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <p><a href=\"/admin/products/add\">Add product</a></p>
        <table border=\"1\">
            <tr>
                <th>#</th>
                <th>name</th>
                <th>description</th>
                <th>price</th>
                <th>image</th>
                <th>actions</th>
            </tr>
            ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 17
            echo "                <tr class=\"";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                echo "rowodd";
            } else {
                echo "roweven";
            }
            echo "\">
                    <td>";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "name", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "description", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "price", array()), "html", null, true);
            echo "</td>
                    <td><img src=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "imagePath", array()), "html", null, true);
            echo "\" width=\"100\"></td>
                    <td>
                        <a href=\"/admin/products/delete/";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo "\">Delete</a>
                        <a href=\"/admin/products/edit/";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo "\">Edit</a>
                    </td>
                </tr>
            ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 29
            echo "                <tr><td colspan=\"6\">You have no products</td></tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "        </table>

";
    }

    public function getTemplateName()
    {
        return "admin/products_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 31,  119 => 29,  102 => 25,  98 => 24,  93 => 22,  89 => 21,  85 => 20,  81 => 19,  77 => 18,  68 => 17,  50 => 16,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Products list{% endblock %}

{% block content %}
    <p><a href=\"/admin/products/add\">Add product</a></p>
        <table border=\"1\">
            <tr>
                <th>#</th>
                <th>name</th>
                <th>description</th>
                <th>price</th>
                <th>image</th>
                <th>actions</th>
            </tr>
            {% for p in list %}
                <tr class=\"{% if loop.index is odd %}rowodd{% else %}roweven{% endif %}\">
                    <td>{{loop.index}}</td>
                    <td>{{p.name}}</td>
                    <td>{{p.description}}</td>
                    <td>{{p.price}}</td>
                    <td><img src=\"{{p.imagePath}}\" width=\"100\"></td>
                    <td>
                        <a href=\"/admin/products/delete/{{p.id}}\">Delete</a>
                        <a href=\"/admin/products/edit/{{p.id}}\">Edit</a>
                    </td>
                </tr>
            {% else %}
                <tr><td colspan=\"6\">You have no products</td></tr>
            {% endfor %}
        </table>

{% endblock %}
", "admin/products_list.html.twig", "C:\\xampp\\htdocs\\ipd\\slimshop\\templates\\admin\\products_list.html.twig");
    }
}
